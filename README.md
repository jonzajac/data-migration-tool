# README

## What this is

This is a tool to assist with migrating a large amount of data from an excel spreadsheet into a RDBMS. It does very little else, and that is by design.

## What this is not

This isn't meant to "just work" once you git clone and build. It will take some effort to customize the tool to your needs. 

## FAQ

### What RDBMS products can I use with this?

 * SQL Server
 * MySql
 * PostgreSQL
 * Oracle
 * Firebird
 
### In what scenarios would I use this?

This tool is best when you need to do regular data imports, where the schema of the source and target will change little or not at all. 

It's probably only useful where there isn't a 1:1 mapping between the Excel spreadsheet and the target database - that is, when the spreadsheet data is getting dumped into multiple objects. 

### Why Excel? 

Excel will never go away. Developers will be importing Excel spreadsheet data into RDBMS products for years to come. 