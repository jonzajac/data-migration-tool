﻿using ServiceStack.DataAnnotations;

namespace DMTool.Models
{
    public class Company
    {
        [AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
