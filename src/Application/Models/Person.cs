﻿using ServiceStack.DataAnnotations;

namespace DMTool.Models
{
    public class Person
    {
        [AutoIncrement] 
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        [References(typeof(Company))]
        public int CompanyId { get; set; }
    }
}
