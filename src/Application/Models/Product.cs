﻿using ServiceStack.DataAnnotations;

namespace DMTool.Models
{
    public class Product
    {
        [AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [References(typeof(Company))]
        public int CompanyId { get; set; }
    }
}
