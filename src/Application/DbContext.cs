﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;

namespace DMTool
{
    public class DbContext
    {
        public static OrmLiteConnectionFactory Initialize()
        {
            return new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["DbContext"].ConnectionString, SqlServerDialect.Provider);
        }
    }
}
