﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using BloomFilter;
using DMTool;
using DMTool.Models;
using ServiceStack.OrmLite;

namespace Application
{
    class Program
    {
        private static string _sourceConnectionString;
        private static string _sourceSpreadsheetName;
        private static OrmLiteConnectionFactory _targetDb;

        static void Main(string[] args)
        {
            Initialize();
            PreProcess();
            Process();
        }

        static void Initialize()
        {
            _sourceConnectionString = ConfigurationManager.AppSettings["dataSource"];
            _sourceSpreadsheetName = ConfigurationManager.AppSettings["spreadsheetIdentifier"];
            _targetDb = DbContext.Initialize();
        }

        static void PreProcess()
        {
            if (_sourceConnectionString == null || String.IsNullOrEmpty(_sourceConnectionString))
            {
                throw new NullReferenceException("Source connection string in the App.config looks to be null. This needs to be populated to proceed");
            }

            if (_sourceSpreadsheetName == null || String.IsNullOrEmpty(_sourceSpreadsheetName))
            {
                throw new NullReferenceException("Source spreadsheet name in the App.config looks to be null. This needs to be populated to proceed.");
            }

            // Database schema creation
            //  For testing purposes, I'll keep this in here, but you may need to uncomment this

            using (IDbConnection db = _targetDb.OpenDbConnection())
            {
                db.CreateTableIfNotExists<Company>();
                db.CreateTableIfNotExists<Person>();
                db.CreateTableIfNotExists<Product>();
            }
        }

        static void Process()
        {
            OleDbConnection xlsConn = new OleDbConnection(_sourceConnectionString);
            xlsConn.Open();

            EnumerableRowCollection<DataRow> sourceDataRows;
            DataSet sourceDataSet = new DataSet();
            OleDbDataAdapter sourceCmd;

            StringBuilder sourceDataQuery = new StringBuilder();
            sourceDataQuery.AppendFormat("SELECT * FROM [{0}$]",
                                         _sourceSpreadsheetName);

            sourceCmd = new OleDbDataAdapter(sourceDataQuery.ToString(), xlsConn);
            sourceCmd.Fill(sourceDataSet, _sourceSpreadsheetName);

            sourceDataRows = sourceDataSet.Tables[_sourceSpreadsheetName].AsEnumerable();

            // Initialize a bloom filter
            //  We're using a bloom filter to identify any duplicate rows before they are inserted
            //  into the database. We can't assume that the data has been deduped before working
            //  with it - in my experience, it often isn't, even if you're told otherwise.
            //
            //  This isn't a perfect approach - for example, if we have a duplicate data item
            //  in rows 3 and 300, the item in row 3 will get inserted but the item in row 300
            //  will be a positive match and not get inserted. However, what if the data item
            //  in row 3 was the dupe and it was row 300 that we wanted? 
            //
            //  A potential to do is to scan the entire dataset checking for dupes before any data is
            //  committed to the database. 

            int bloomFilterCapacity = sourceDataRows.Count();

            // Lower the acceptable false positive rate to 0.01%
            //  The goal here is to not flag so many potentially duplicate items as to annoy the user.
            //  The tradeoff with this approach is we're allocating more memory space for the bloom filter. 
            //  This shouldn't be a big deal with a reasonably sized dataset (<1m rows), but if your dataset 
            //  is above that then you may find it necessary to bump up the rate.
            float bloomFilterFalsePositiveRate = 0.001F;

            Filter<string> bloomFilter = new Filter<string>(bloomFilterCapacity, bloomFilterFalsePositiveRate, null);

            using (IDbConnection db = _targetDb.OpenDbConnection())
            {
                // Process each row one at a time
                foreach (DataRow row in sourceDataRows)
                {
                    using (var trans = db.OpenTransaction(IsolationLevel.ReadCommitted))
                    {
                        // This is to catch the rows where there is no content, but Excel still thinks it's a row
                        // with data in it. We're filtering on the company name because all of our other data 
                        // revolves around the company not being null
                        if (!String.IsNullOrWhiteSpace(row["Company Name"].ToString()))
                        {
                            // Process company 
                            //  First create a new object with data from the xls file 
                            var company = new Company
                            {
                                Name = row["Company Name"].ToString().Trim(),
                                Description = row["Company Description"].ToString().Trim()
                            };

                            // Check if the company exists in the target database. Again, not a perfect 
                            // way of matching - data tends to get messed up. Probably a better way of doing
                            // this is doing fuzzy matching
                            bool companyExists = db.Select<Company>("Name = {0}", company.Name).Any();

                            int companyId = 0;
                            // We will populate this based on the result of either the insert or update

                            if (bloomFilter.Contains(company.Name))
                            {
                                // Flag this entry to the user 
                            }
                            else
                            {
                                if (companyExists)
                                {
                                    company.Id = db.SingleOrDefault<Company>("Name = {0}", company.Name).Id;

                                    // Company exists, so we should update the company's information
                                    db.Save(company);

                                    companyId = company.Id;
                                }
                                else
                                {
                                    // Company probably doesn't exist
                                    db.Save(company);

                                    // Keep track of this addition by adding it to the bloom filter
                                    bloomFilter.Add(company.Name);

                                    companyId = (int)db.GetLastInsertId();
                                }
                            }

                            // Owner details

                            var owner = new Person
                            {
                                FirstName = row["Owner First Name"].ToString().Trim(),
                                LastName = row["Owner Last Name"].ToString().Trim(),
                                Age = ConvertToInt(row, "Owner Age", 0),
                                CompanyId = companyId
                            };

                            db.Save(owner);

                            // Employee details

                            var employee = new Person
                            {
                                FirstName = row["Employee First Name"].ToString().Trim(),
                                LastName = row["Employee Last Name"].ToString().Trim(),
                                Age = ConvertToInt(row, "Employee Age", 0),
                                CompanyId = companyId
                            };

                            db.Save(employee);

                            // Product 1

                            var product1 = new Product
                            {
                                Name = row["Product 1 Name"].ToString().Trim(),
                                Description = row["Product 1 Description"].ToString().Trim(),
                                CompanyId = companyId
                            };

                            db.Save(product1);

                            var product2 = new Product
                            {
                                Name = row["Product 2 Name"].ToString().Trim(),
                                Description = row["Product 2 Description"].ToString().Trim(),
                                CompanyId = companyId
                            };

                            db.Save(product2);

                            // Commit transaction
                            trans.Commit();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Helper method to safely convert objects into ints
        /// </summary>
        /// <param name="objToConvert">DataRow to convert</param>
        /// <param name="key">key of object</param>
        /// <param name="defaultValue">value to return if null</param>
        /// <returns>Converted integer</returns>
        private static int ConvertToInt(DataRow objToConvert, string key, int defaultValue)
        {
            if (objToConvert[key] is DBNull)
            {
                return defaultValue;
            }

            return Convert.ToInt32(objToConvert[key]);
        }
    }
}
